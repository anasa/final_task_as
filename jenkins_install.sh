#!/bin/bash
       echo "Begin setup"

       sudo amazon-linux-extras install epel -y
       sudo yum update -y
       sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
       sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
       sudo yum upgrade
       sudo yum install jenkins java-1.8.0-openjdk-devel -y
       sudo systemctl daemon-reload
       sudo systemctl start jenkins
       sudo systemctl status jenkins

       echo "Jenkins setup completed"

       sudo amazon-linux-extras install docker
       sudo yum install docker
       sudo service docker start

       echo "Docker setup completed"
       sudo usermod -a -G docker ec2-user
       sudo usermod -a -G docker jenkins
       sudo service jenkins restart
       sudo systemctl daemon-reload
       sudo service docker restart
       echo "Permissions setup successfully"

       sudo yum install git -y
       echo "Installing docker-compose"
       sudo curl -L "https://github.com/docker/compose/releases/download/1.29.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
       sudo chmod +x /usr/local/bin/docker-compose
       echo "Install completed"
