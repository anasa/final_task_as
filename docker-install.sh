#!/bin/bash

       sudo yum update -y
       sudo yum install jenkins java-1.8.0-openjdk-devel -y
       sudo systemctl daemon-reload

       sudo amazon-linux-extras install docker
       sudo service docker start

       sudo usermod -a -G docker ec2-user
       sudo chkconfig docker on

       sudo yum install git -y

       echo "Installing docker-compose"
       sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
       sudo chmod +x /usr/local/bin/docker-compose
