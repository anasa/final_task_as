provider "aws" {
  access_key = "--"
  secret_key = "--"
  region     = "eu-central-1"
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "vpc-task" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name    = "vpc-task"
    Creator = "AS"
  }
}

resource "aws_internet_gateway" "int-gw" {
  vpc_id = aws_vpc.vpc-task.id

  tags = {
    Name    = "int-gw"
    Creator = "AS"
  }
}

resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.vpc-task.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.int-gw.id
  }

  tags = {
    Name    = "public-route-table"
    Creator = "AS"
  }
}

resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.vpc-task.id

  tags = {
    Name    = "private-route-table"
    Creator = "AS"
  }
}

resource "aws_subnet" "public-a" {
  vpc_id                  = aws_vpc.vpc-task.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name    = "public-subnet-a"
    Creator = "AS"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public-a.id
  route_table_id = aws_route_table.public-route-table.id
}

resource "aws_subnet" "public-b" {
  vpc_id                  = aws_vpc.vpc-task.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name    = "public-subnet-b"
    Creator = "AS"
  }
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.public-b.id
  route_table_id = aws_route_table.public-route-table.id
}

resource "aws_subnet" "private-a" {
  vpc_id                  = aws_vpc.vpc-task.id
  cidr_block              = "10.0.30.0/24"
  availability_zone       = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = true

  tags = {
    Name    = "private-subnet-a"
    Creator = "AS"
  }
}

resource "aws_route_table_association" "pr-a" {
  subnet_id      = aws_subnet.private-a.id
  route_table_id = aws_route_table.private-route-table.id
}

resource "aws_subnet" "private-b" {
  vpc_id                  = aws_vpc.vpc-task.id
  cidr_block              = "10.0.20.0/24"
  availability_zone       = data.aws_availability_zones.available.names[1]
  map_public_ip_on_launch = true

  tags = {
    Name    = "private-subnet-b"
    Creator = "AS"
  }
}

resource "aws_route_table_association" "pr-b" {
  subnet_id      = aws_subnet.private-b.id
  route_table_id = aws_route_table.private-route-table.id
}

resource "aws_security_group" "allow_ssh_http" {
  name        = "allow_ssh_http"
  description = "Allow ssh and http connections"
  vpc_id      = aws_vpc.vpc-task.id


  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name    = "allow_ssh_http"
    Creator = "AS"
  }
}

resource "aws_network_interface" "app_server_ni" {
  subnet_id       = aws_subnet.public-a.id
  private_ips     = ["10.0.1.10"]
  security_groups = [aws_security_group.allow_ssh_http.id]

  tags = {
    Name    = "app_server_ni"
    Creator = "AS"
  }
}

resource "aws_instance" "app_server" {
  ami                  = "ami-05d34d340fb1d89e5"
  instance_type        = "t2.micro"
  iam_instance_profile = "ECRforEC2"
  key_name             = "key-pair-task"
  user_data            = file("docker-install.sh")

  network_interface {
    network_interface_id = aws_network_interface.app_server_ni.id
    device_index         = 0
  }
  tags = {
    Name    = "App server"
    Creator = "AS"
  }
}

resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins_master_server"
  description = "Jenkins master servers traffic"
  vpc_id      = aws_vpc.vpc-task.id

  ingress {
    description = "HTTP"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Custom"
    from_port   = 50000
    to_port     = 50000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name    = "jenkins_master_server"
    Creator = "AS"
  }
}

resource "aws_network_interface" "jenkins_server_ni" {
  subnet_id       = aws_subnet.public-a.id
  private_ips     = ["10.0.1.100"]
  security_groups = [aws_security_group.jenkins-sg.id]

  tags = {
    Name    = "jenkins_server_ni"
    Creator = "AS"
  }
}

resource "aws_instance" "jenkins_server" {
  ami                  = "ami-05d34d340fb1d89e5"
  instance_type        = "t2.micro"
  iam_instance_profile = "ECRforEC2"
  key_name             = "key-pair-task"

  user_data = file("jenkins_install.sh")

  network_interface {
    network_interface_id = aws_network_interface.jenkins_server_ni.id
    device_index         = 0
  }

  tags = {
    Name    = "Jenkins master server"
    Creator = "AS"
  }
}

resource "aws_lb" "ALB" {
  name               = "load-balancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = [aws_subnet.public-a.id, aws_subnet.public-b.id]

  enable_deletion_protection = true

  tags = {
    Name    = "ALB"
    Creator = "AS"
  }
}
resource "aws_security_group" "alb_sg" {
  name        = "SG for ALB"
  description = "Security group for ALB"
  vpc_id      = aws_vpc.vpc-task.id


  ingress {
    description = "HTTP over SSL"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name    = "SG for ALB"
    Creator = "AS"
  }
}

resource "aws_lb_target_group" "alb_tg" {
  name     = "ALB-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc-task.id
}

resource "aws_lb_target_group_attachment" "lb_target_group_attachment" {
  target_group_arn = aws_lb_target_group.alb_tg.arn
  target_id        = aws_instance.app_server.id
  port             = 80
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.ALB.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_tg.arn
  }
}

resource "aws_db_subnet_group" "database_subnet" {
  name       = "main"
  subnet_ids = [aws_subnet.private-a.id, aws_subnet.private-b.id]

  tags = {
    Name    = "Database subnet group"
    Creator = "AS"
  }
}

resource "aws_db_instance" "RDS" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t3.micro"
  username               = "rds_root_user"
  password               = "fTrrro3340drgh"
  name                   = "RDS_db"
  parameter_group_name   = "default.mysql5.7"
  vpc_security_group_ids = [aws_security_group.RDS-sg.id]
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.database_subnet.id
  tags = {
    Creator = "AS"
  }
}

resource "aws_security_group" "RDS-sg" {
  name        = "SG for RDS"
  description = "Security group for RDS"
  vpc_id      = aws_vpc.vpc-task.id


  ingress {
    description = "SQL"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name    = "RDS_trafic"
    Creator = "AS"
  }
}


output "web_instance_ip_addr" {
  value       = aws_instance.app_server.public_ip
  description = "The public IP address of the main server instance."
}

output "jenkins_instance_ip_addr" {
  value       = aws_instance.jenkins_server.public_ip
  description = "The public IP address of the jenkins instance."
}

output "load_balancer_dns" {
  value       = aws_lb.ALB.dns_name
  description = "The public DNS load balancer."
}

output "RDS_endpoint" {
  value       = aws_db_instance.RDS.endpoint
  description = "RDS_endpoint"
}

output "RDS_root" {
  value       = aws_db_instance.RDS.username
  description = "RDS_root"
}

output "RDS_name" {
  value       = aws_db_instance.RDS.name
  description = "RDS_name"
}
